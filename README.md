# 44-564 H03 Working with Python

Basic python introduction and concepts needed 
in preparation for working with MapReduce

# Requirements

- Download and install Python 2.6.6 
- Git for Windows
- TortoiseGit
- Open Command Window Here as Administrator
- Notepad++ for text editing

# References

See Udacity "Introduction to Hadoop and MapReduce"
- https://classroom.udacity.com/courses/ud617/